import React from 'react';
import PropTypes from 'prop-types';
import 'highlightjs/styles/atom-one-dark.css';

import MarkdownIt from 'markdown-it';
import markdownEmojies from 'markdown-it-emoji';
import markdownAbbr from 'markdown-it-abbr';
import markdownitMark from 'markdown-it-mark';
import hljs from 'highlightjs';
import twemoji from 'twemoji';

const Markdown = props => {
  const md = new MarkdownIt({
    html: true,
    linkify: true,
    typographer: true,
    highlight(str, lang) {
      if (lang && hljs.getLanguage(lang)) {
        try {
          return hljs.highlight(lang, str).value;
        } catch (__) {
          console.log('Error in MD!');
        }
      }
      return ''; // use external default escaping
    },
  })
    .use(markdownEmojies)
    .use(markdownAbbr)
    .use(markdownitMark);
  md.renderer.rules.emoji = function(token, idx) {
    return twemoji.parse(token[idx].content);
  };
  return (
    <div
      className="markdown"
      dangerouslySetInnerHTML={{ __html: md.render(props.children) }}
    />
  );
};

Markdown.propTypes = {
  children: PropTypes.string.isRequired,
};

export default Markdown;
