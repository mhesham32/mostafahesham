import React from 'react';
import Twitter from 'react-icons/lib/ti/social-twitter';
import Facebook from 'react-icons/lib/ti/social-facebook';
import Linkedin from 'react-icons/lib/ti/social-linkedin';
import Google from 'react-icons/lib/ti/social-google-plus';
import {
  FacebookShareButton,
  TwitterShareButton,
  GooglePlusShareButton,
  LinkedinShareButton,
} from 'react-share';
import Types from 'prop-types';

const desc = 'Have a look at this project from Mostafa Hesham';

const Share = props => (
  <div className={`${props.parentClassName}__share`}>
    <div
      className={`${props.parentClassName}__share--icon ${
        props.parentClassName
      }__share--facebook`}
    >
      <FacebookShareButton url={props.postUrl} quote={desc}>
        <Facebook />
      </FacebookShareButton>
    </div>
    <div
      className={`${props.parentClassName}__share--icon ${
        props.parentClassName
      }__share--twitter`}
    >
      <TwitterShareButton
        url={props.postUrl}
        title={document.title}
        via="mhesham32"
      >
        <Twitter />
      </TwitterShareButton>
    </div>
    <div
      className={`${props.parentClassName}__share--icon ${
        props.parentClassName
      }__share--linkedin`}
    >
      <LinkedinShareButton
        url={props.postUrl}
        title={document.title}
        description={desc}
      >
        <Linkedin />
      </LinkedinShareButton>
    </div>
    <div
      className={`${props.parentClassName}__share--icon ${
        props.parentClassName
      }__share--google`}
    >
      <GooglePlusShareButton url={props.postUrl}>
        <Google />
      </GooglePlusShareButton>
    </div>
  </div>
);

Share.propTypes = {
  parentClassName: Types.string.isRequired,
  postUrl: Types.string.isRequired,
};

export default Share;
