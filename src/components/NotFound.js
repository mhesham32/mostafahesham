import React from 'react';
import { Link } from 'react-router-dom';
import notFoundBackground from '../assets/notFound.jpeg';

const styles = {
  backgroundImage: `linear-gradient(to right bottom,
    rgba(7, 71, 166,0.8),
    rgba(0, 101, 255,0.8)
  ),
  url(${notFoundBackground})`,
};

const NotFound = () => (
  <div className="not-found" style={styles}>
    <h1 className="not-found__heading">404</h1>
    <p className="not-found__description">
      Sorry the page you are trying to access isn't here any more!
    </p>
    <div className="not-found__button">
      <Link to="/">Visit Home page!</Link>
    </div>
  </div>
);

export default NotFound;
