import React from 'react';
import Types from 'prop-types';

const Form = ({ onChange, name, email, message, subject, onSubmit }) => (
  <form className="contact-me__form" onChange={onChange} onSubmit={onSubmit}>
    <div className="contact-me__group">
      <input
        id="name"
        name="name"
        type="text"
        className="contact-me__input"
        value={name}
        placeholder="your name"
        required
      />
      <label htmlFor="name" className="contact-me__label">
        your name
      </label>
    </div>
    <div className="contact-me__group">
      <input
        id="subject"
        name="subject"
        type="text"
        className="contact-me__input"
        value={subject}
        placeholder="email subject"
        required
      />
      <label htmlFor="subject" className="contact-me__label">
        email subject
      </label>
    </div>
    <div className="contact-me__group">
      <input
        id="email"
        name="email"
        type="email"
        className="contact-me__input"
        value={email}
        placeholder="your email"
        required
      />
      <label htmlFor="email" className="contact-me__label">
        your email
      </label>
    </div>
    <div className="contact-me__group">
      <textarea
        name="message"
        id="textarea"
        className="contact-me__textarea"
        rows="15"
        value={message}
        required
      />
      <label htmlFor="textarea" className="contact-me__textarea-label">
        your message
      </label>
    </div>
    <button type="submit" className="contact-me__button">
      Send Message
    </button>
  </form>
);

Form.propTypes = {
  onChange: Types.func.isRequired,
  onSubmit: Types.func.isRequired,
  name: Types.string.isRequired,
  email: Types.string.isRequired,
  message: Types.string.isRequired,
  subject: Types.string.isRequired,
};

export default Form;
