import React from 'react';
import $ from 'jquery';
import Form from './Form';

class ContactMe extends React.Component {
  state = {
    name: '',
    email: '',
    message: '',
    subject: '',
    isSent: false,
    isSuccess: false,
  };

  onChange = ({ target }) => {
    this.setState({ [target.name]: target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const url = 'https://formcarry.com/s/KrlpObw9zm9';

    const { name, email, message, subject } = this.state;

    $.ajax({
      type: 'POST',
      dataType: 'json',
      url,
      data: { name, email, message, subject },
      success: function(response) {
        if (response.status === 'success') {
          this.setState({ isSent: true, isSuccess: true });
        } else {
          this.setState({ isSent: true, isSuccess: false });
        }
      }.bind(this),
    });
  };

  renderIfSent = isSuccess =>
    isSuccess ? (
      <div>
        <img
          src="https://octodex.github.com/images/daftpunktocat-thomas.gif"
          alt=""
          className="contact-me__image"
        />
        <h3 className="contact-me__respond">
          Your message was sent successfuly
        </h3>
      </div>
    ) : (
      <div>
        <img
          src="https://octodex.github.com/images/herme-t-crabb.png"
          alt=""
          className="contact-me__image"
        />
        <h3 className="contact-me__respond">
          Something went wrong please try again later
        </h3>
      </div>
    );

  render() {
    if (this.state.isSent) {
      return this.renderIfSent(this.state.isSuccess);
    }
    return (
      <Form
        onChange={this.onChange}
        name={this.state.name}
        email={this.state.email}
        message={this.state.message}
        subject={this.state.subject}
        isSent={this.state.isSent}
        isSuccess={this.state.isSuccess}
        onSubmit={this.onSubmit}
      />
    );
  }
}

export default ContactMe;
