import React from 'react';
import PropTypes from 'prop-types';

const Paragraph = ({ color, children }) => (
  <p className="profile__paragraph" style={{ color }}>
    {children}
  </p>
);

Paragraph.propTypes = {
  color: PropTypes.string,
  children: PropTypes.string.isRequired,
};

Paragraph.defaultProps = {
  color: 'rgb(222, 235, 255)',
};

export default Paragraph;
