import React from 'react';
import avatar from '../../assets/avatar.jpg';
import Paragraph from './Paragraph';
import H1 from './H1';

const AboutMe = () => (
  <div className="about-me" id="about-me">
    <div className="about-me__avatar">
      <img src={avatar} alt="" className="about-me__avatar--image" />
      <div className="about-me__avatar--info">
        <h1 className="about-me__avatar--name">Mostafa Hesham</h1>
        <a
          href="https://drive.google.com/file/d/1IpcWQtVXLowOGWLiL3rFsuEe1nBbd11u/view"
          className="about-me__avatar--button"
        >
          Download Resume
        </a>
      </div>
    </div>
    <div className="about-me__content">
      <H1>About me</H1>
      <Paragraph>
        I'm Mostafa from Beni Suef,Egypt a student at faculty of commerce in my
        graduation year wish me luck. I have joined the world of web development
        in 2016 and since that I'm nearly coding every day.
      </Paragraph>
      <H1>More...</H1>
      <Paragraph>
        I have my development education online from various sources like
        codecademy , freecodecamp, udacity and udemy and I have to give credits
        for codecademy and freecodecamp they push me to continue and have
        amazing community.
      </Paragraph>
      <Paragraph>
        if you attempt to learn web development I recommend to start with
        codecademy then freecodecamp and any resources from your choices.
      </Paragraph>
      <Paragraph>
        I'm working with the MERN stack which consists of MongoDB, Express,
        React and Node.js But not all times sometimes I use Google Firebase
        which I like so much I'm using Firebase here in my website and created a
        cms like system which was easy and fun.
      </Paragraph>
      <H1>When Not coding</H1>
      <Paragraph>
        I do a lot of things when i'm not coding it may be hanging out with
        friends , playing chess or League of Legends , Watching a movie (I love
        movies), learning new stuff the last thing I could do is studing
        commerce :) .
      </Paragraph>
      <H1>Future plans</H1>
      <Paragraph>
        I really love JavaScript and the web development and I want to improve
        it to be like native and to be used everywhere with efficiency , So i'm
        looking forward to learn about PWAs (progressive web apps) and gain the
        Google's mobile web certificate.
      </Paragraph>
    </div>
  </div>
);

export default AboutMe;
