import React from 'react';

const H1 = props => <h1 className="about-me__heading">{props.children}</h1>;

export default H1;
