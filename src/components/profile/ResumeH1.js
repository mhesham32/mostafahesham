import React from 'react';

const ResumeH1 = ({ children }) => (
  <h1 className="resume__heading">{children}</h1>
);

export default ResumeH1;
