import React from 'react';
import ResumeH1 from './ResumeH1';
import Paragraph from './Paragraph';
import ResumeInfo from './ResumeInfo';

const ResumeParagraph = ({ children }) => (
  <Paragraph color="rgb(66, 82, 110)">{children}</Paragraph>
);

const Resume = () => (
  <div className="resume" id="resume">
    <h1 className="resume__title">Resume</h1>
    <ResumeInfo />
    <ResumeH1>Professional Summary</ResumeH1>
    <ResumeParagraph>
      Knowledgeable Front End Developer adept at creating successful websites
      that meet customer needs. Specializing in collaborating with possible
      customers to gather requirements, produce plans and improve designs for
      usability and functionality. Fully proficient in HTML, CSS and JavaScript.
    </ResumeParagraph>
    <ResumeH1>Skills</ResumeH1>
    <ul className="resume__list">
      <li>
        <ResumeParagraph>
          Strong knowledge of HTML, CSS/SASS, and Javascript.
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Strong knowledge of the React library and its ecosystem.
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Ability to work with building tools like Webpack and Gulp.
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Able to work in a full stack projects using Node.js or using a service
          like Google Firebase.
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Ability to Tackle tough design and product problems.
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Using patterns like BEM in styling or use css in js for the seek of
          reducing the boilerplate code and reusability.
        </ResumeParagraph>
      </li>
    </ul>
    <ResumeH1>Work History</ResumeH1>
    <ResumeParagraph>I work as freelancer since 2017 .</ResumeParagraph>
    <ResumeH1>Education</ResumeH1>
    <ResumeParagraph>
      Freecodecamp website and other online courses
    </ResumeParagraph>
    <ResumeH1>Certifications</ResumeH1>
    <ul className="resume__list">
      <li>
        <ResumeParagraph>Freecodecamp Responsive Web Design</ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>
          Freecodecamp JavaScript Algorithms And Data Structures
        </ResumeParagraph>
      </li>
      <li>
        <ResumeParagraph>Freecodecamp Front End Libraries</ResumeParagraph>
      </li>
    </ul>
  </div>
);

export default Resume;
