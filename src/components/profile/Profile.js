import React from 'react';
import AboutMe from './AboutMe';
import Resume from './Resume';

class Profile extends React.Component {
  componentWillMount() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflow = 'hidden';
  }

  componentWillUnmount() {
    const body = document.getElementsByTagName('body')[0];
    body.style.overflow = 'scroll';
  }

  render() {
    return (
      <div className="profile">
        <AboutMe />
        <Resume />
      </div>
    );
  }
}

export default Profile;
