import React from 'react';
import avatar from '../../assets/avatar.jpg';

const ResumeInfo = () => (
  <div className="resume__info">
    <img src={avatar} alt="" className="resume__info--avatar" />
    <div className="resume__info--text">
      <h1 className="resume__info--text--name">Mostafa Hesham</h1>
      <h2 className="resume__info--text--bold">+201276033175</h2>
      <h2 className="resume__info--text--bold">m.hesham.32@live.com</h2>
      <h2 className="resume__info--text--bold">
        El Wafd st, Somosta, Beni suef, Egypt
      </h2>
      <h2 className="resume__info--text--bold">62612</h2>
    </div>
  </div>
);

export default ResumeInfo;
