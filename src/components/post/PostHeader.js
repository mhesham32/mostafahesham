import React from 'react';
import PropTypes from 'prop-types';
import Eye from 'react-icons/lib/fa/eye';
import Github from 'react-icons/lib/fa/github';

const PostHeader = ({ title, previewLink, githubLink, headerImage }) => (
  <div
    className="post__header"
    style={{
      backgroundImage: `linear-gradient(to right bottom ,rgba(250,251,252,0.3),rgba(12,12,13,0.3)),url(${headerImage})`,
    }}
  >
    <h1 className="post__header--title">{title}</h1>
    <div className="post__header--buttons">
      <a
        target="_blank"
        href={previewLink}
        className="post__header--buttons--preview"
      >
        <Eye />
        Preview
      </a>
      <a
        href={githubLink}
        target="_blank"
        className="post__header--buttons--github"
      >
        <Github />
        view Code
      </a>
    </div>
  </div>
);

PostHeader.propTypes = {
  title: PropTypes.string.isRequired,
  previewLink: PropTypes.string,
  githubLink: PropTypes.string,
  headerImage: PropTypes.string.isRequired,
};

PostHeader.defaultProps = {
  previewLink: '#',
  githubLink: '#',
};

export default PostHeader;
