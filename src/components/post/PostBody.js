import React from 'react';
import PropTypes from 'prop-types';

import PostHeader from './PostHeader';
import Markdown from '../common/Markdown';

const PostBody = ({ content, desc }) => (
  <div className="post__content--container">
    <div className="post__content">
      <Markdown>{content}</Markdown>
    </div>
  </div>
);

PostBody.propTypes = {
  content: PropTypes.string.isRequired,
};

export default PostBody;
