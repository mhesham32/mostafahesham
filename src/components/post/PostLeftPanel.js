import React from 'react';
import PropTypes from 'prop-types';

import Tag from '../Admin/Tag';
import Share from '../common/Share';

const PostLeftPanel = ({ tags, postUrl, postDate }) => (
  <div className="post-left-panel">
    <PostTags tags={tags} />
    <Share parentClassName="post" postUrl={postUrl} />
    <div className="post-left-panel__date">published at: {postDate}</div>
  </div>
);

PostLeftPanel.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
  postUrl: PropTypes.string.isRequired,
  postDate: PropTypes.string.isRequired,
};

const PostTags = ({ tags }) => (
  <React.Fragment>
    <h1 className="post-left-panel__heading">post tags</h1>
    <div className="post-left-panel__tags">
      {tags.map(tag => <Tag name={tag} key={`#${tag}`} />)}
    </div>
  </React.Fragment>
);

PostTags.propTypes = {
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default PostLeftPanel;
