import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import getPostBySlug from '../../selectors/getPostBySlug';
import getPostDate from '../../selectors/getPostDate';
import Loader from '../common/Loader';
import PostHeader from './PostHeader';
import PostBody from './PostBody';
import PostLeftPanel from './PostLeftPanel';
import PostRightPanel from './PostRightPanel';

class Post extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    headerImage: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    tags: PropTypes.array.isRequired,
    loading: PropTypes.bool.isRequired,
    postDate: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    posts: PropTypes.arrayOf(PropTypes.object).isRequired,
    previewLink: PropTypes.string.isRequired,
    githubLink: PropTypes.string.isRequired,
  };

  componentDidMount() {
    const { loading, title } = this.props;

    if (!loading && title && title !== '') {
      document.title = title;
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.loading && nextProps.title !== this.props.title) {
      document.title = nextProps.title;
    }
  }

  url = window.location.href;

  render() {
    const {
      title,
      desc,
      headerImage,
      content,
      tags,
      loading,
      postDate,
      posts,
      id,
      previewLink,
      githubLink,
    } = this.props;
    return (
      <div className="post">
        {loading ? (
          <Loader />
        ) : (
          <Fragment>
            <PostHeader
              title={title}
              headerImage={headerImage}
              previewLink={previewLink}
              githubLink={githubLink}
            />
            <div className="post__desc">
              <p>{desc}</p>
            </div>
            <div className="post-box">
              <PostLeftPanel
                tags={tags}
                postDate={postDate}
                postUrl={this.url}
              />
              <PostBody content={content} />
              <PostRightPanel
                tags={tags}
                loading={loading}
                posts={posts}
                id={id}
              />
            </div>
          </Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => ({
  ...getPostBySlug(state, ownProps.match.params.slug), // returns the post Object
  postDate: getPostDate(
    state,
    getPostBySlug(state, ownProps.match.params.slug)
  ),
  loading: state.postsMetadata.loading,
  posts: state.posts,
});

export default connect(mapStateToProps, undefined)(Post);
