import React from 'react';
import PropTypes from 'prop-types';

import PostHeader from './PostHeader';

const HeaderImage = ({ title, headerImage }) => (
  <div
    className="post__header"
    style={{
      backgroundImage: `linear-gradient(to right bottom ,rgba(250,251,252,0.3),rgba(12,12,13,0.3)),url(${headerImage})`,
    }}
  >
    <h1 className="post__header--title">{title}</h1>
    <PostHeader />
  </div>
);

HeaderImage.propTypes = {
  title: PropTypes.string.isRequired,
  headerImage: PropTypes.string.isRequired,
};

export default HeaderImage;
