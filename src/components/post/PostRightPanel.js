import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SimilarPost from './SimilarPost';
import Loader from '../common/Loader';
import { runInThisContext } from 'vm';

export default class PostRightPanel extends Component {
  static propTypes = {
    tags: PropTypes.arrayOf(PropTypes.string).isRequired,
    id: PropTypes.string.isRequired,
    posts: PropTypes.arrayOf(PropTypes.object).isRequired,
    loading: PropTypes.bool.isRequired,
  };

  isSimilarEnough = (defaultTags, tags) => {
    const sharedItems = [];
    defaultTags.forEach(item => {
      tags.forEach(tagItem => {
        if (tagItem === item) {
          sharedItems.push(item);
        }
      });
    });
    if (sharedItems.length > defaultTags.length / 2) {
      return true;
    }
    return false;
  };

  findSimilar = (tags, posts, id) =>
    posts.filter(post => {
      if (id !== post.id) {
        return this.isSimilarEnough(tags, post.tags);
      }
    });

  renderPosts = (loading, posts, tags, id) => (
    <React.Fragment>
      {loading ? (
        <Loader />
      ) : (
        this.findSimilar(tags, posts, id).map(post => (
          <SimilarPost
            title={post.title}
            headerImage={post.headerImage}
            slug={post.slug}
            key={post.id}
          />
        ))
      )}
    </React.Fragment>
  );

  render() {
    const { loading, posts, tags, id } = this.props;
    return (
      <div className="post-right-panel">
        <h1 className="post-right-panel__header">Similar Posts</h1>
        <div className="post-right-panel__posts">
          {this.findSimilar(tags, posts, id).length < 1 ? (
            <div className="post-right-panel__no-similar">
              <h1 className="post-right-panel__no-similar--title">
                There is no similar posts
              </h1>
              <img
                src="https://octodex.github.com/images/stormtroopocat.jpg "
                alt=""
                className="post-right-panel__no-similar--image"
              />
            </div>
          ) : (
            this.renderPosts(loading, posts, tags, id)
          )}
        </div>
      </div>
    );
  }
}
