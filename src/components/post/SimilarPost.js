import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const SimilarPost = ({ title, headerImage, slug }) => (
  <Link className="similar-post" to={`post/${slug}`}>
    <div
      className="similar-post__background"
      style={{ backgroundImage: `url(${headerImage})` }}
    />
    <h3 className="similar-post__title">{title}</h3>
  </Link>
);

SimilarPost.propTypes = {
  title: PropTypes.string.isRequired,
  headerImage: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
};

export default SimilarPost;
