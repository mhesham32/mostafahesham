import React from 'react';
import PropTypes from 'prop-types';

import StaticIcon from 'react-icons/lib/fa/clipboard';
import InteractiveAppIcon from 'react-icons/lib/fa/cubes';
import FullstackIcon from 'react-icons/lib/fa/database';

const WhatIDoCard = props => (
  <div className="what-i-do__card">
    <div className="what-i-do__card--side">
      <div className="what-i-do__card__front">
        <div className="what-i-do__icon">
          {props.icon ? <props.icon /> : ''}
        </div>
        <h1 className="what-i-do__title">{props.title}</h1>
        <p className="what-i-do__description">{props.description}</p>
      </div>
      <div className="what-i-do__card__back">
        <h1 className="what-i-do__title">{props.title}</h1>
        <div className="what-i-do__button">
          <a href={`projects/${props.link}`}>show projects</a>
        </div>
      </div>
    </div>
  </div>
);

WhatIDoCard.propTypes = {
  icon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};

export default WhatIDoCard;

// TODO:change the description to a better one

export const cardsData = [
  {
    title: 'static web pages',
    icon: StaticIcon,
    description:
      'I build a static web pages for your what ever you need with a nice and responsive Design which fit in a lot of different devices',
    link: 'static-pages',
  },
  {
    title: 'interactive web applications',
    icon: InteractiveAppIcon,
    description:
      'I build a front end interactive applications that are users friendly and users enjoy engaging and interacting with them with a great user experience',
    link: 'interactive-apps',
  },
  {
    title: 'fullstack interactive web applications',
    icon: FullstackIcon,
    description:
      'I build full stack applications that includes back-end development as well as front-end using a server to provide back-end APIs or a service like Google Firebase',
    link: 'fullstack-apps',
  },
];
