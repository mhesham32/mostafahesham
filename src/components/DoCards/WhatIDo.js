import React from 'react';
import WhatIDoCard, { cardsData } from './WhatIDoCard';

const WhatIDo = () => (
  <section className="what-i-do">
    <h1 className="what-i-do__heading">What I do </h1>
    <div className="what-i-do__cards">
      {cardsData.map(card => <WhatIDoCard {...card} key={card.title} />)}
    </div>
  </section>
);

export default WhatIDo;
