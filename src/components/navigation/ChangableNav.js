import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Twitter from 'react-icons/lib/ti/social-twitter';
import Github from 'react-icons/lib/ti/social-github';

export default class ChangableNav extends Component {
  state = {
    isColored: true,
  };

  componentWillMount() {
    if (document.documentElement.scrollTop > 55) {
      this.setState({ isColored: true });
    } else {
      this.setState({ isColored: false });
    }
  }

  componentDidMount() {
    window.onscroll = this.onScroll;
  }

  onScroll = () => {
    if (document.documentElement.scrollTop > 55) {
      this.setState({ isColored: true });
    } else {
      this.setState({ isColored: false });
    }
  };

  render() {
    return (
      <nav className={this.state.isColored ? ' nav colored' : 'nav'}>
        <div className="nav__logo">
          <Link to="/">Mostafa Hesham</Link>
        </div>
        <div className="nav__item">
          <NavLink to="/profile" activeClassName="nav__item--active">
            Profile
          </NavLink>
        </div>
        <div className="nav__item">
          <NavLink
            to={
              this.props.location.pathname.includes('projects')
                ? '/projects/all'
                : '/projects'
            }
            activeClassName="nav__item--active"
          >
            Projects
          </NavLink>
        </div>
        <div className="nav__icon">
          <a href="https://twitter.com/mhesham32" className="nav__link">
            <Twitter />
          </a>
        </div>
        <div className="nav__icon">
          <a href="https://github.com/mhesham32" className="nav__link">
            <Github />
          </a>
        </div>
      </nav>
    );
  }
}
