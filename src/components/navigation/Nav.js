import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import ResponsiveNav from './ResponsiveNav';
import ChangableNav from './ChangableNav';
import ColoredNav from './ColoredNav';

class Nav extends React.Component {
  static propTypes = {
    location: PropTypes.object.isRequired,
  };

  state = {
    viewport: window.innerWidth,
  };

  componentDidMount() {
    window.onresize = this.onResize;
  }

  onResize = () => {
    this.setState({ viewport: window.innerWidth });
  };

  render() {
    // eslint-disable-no-nested-ternary
    if (this.props.location.pathname.includes('admin')) return null;
    return this.state.viewport < 1024 ? (
      <ResponsiveNav />
    ) : this.props.location.pathname === '/' ? (
      <ChangableNav location={this.props.location} />
    ) : (
      <ColoredNav location={this.props.location} />
    );
  }
}

export default withRouter(Nav);
