import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import Twitter from 'react-icons/lib/ti/social-twitter';
import Github from 'react-icons/lib/ti/social-github';

const ColoredNav = props => (
  <nav className=" nav colored">
    <div className="nav__logo">
      <Link to="/">Mostafa Hesham</Link>
    </div>
    <div className="nav__item">
      <NavLink to="/profile" activeClassName="nav__item--active">
        Profile
      </NavLink>
    </div>
    <div className="nav__item">
      <NavLink
        to={
          props.location.pathname.includes('projects')
            ? '/projects/all'
            : '/projects'
        }
        activeClassName="nav__item--active"
      >
        Projects
      </NavLink>
    </div>
    <div className="nav__icon">
      <a href="https://twitter.com/mhesham32" className="nav__link">
        <Twitter />
      </a>
    </div>
    <div className="nav__icon">
      <a href="https://github.com/mhesham32" className="nav__link">
        <Github />
      </a>
    </div>
  </nav>
);

export default ColoredNav;
