import React, { Component } from 'react';
import { Link, NavLink, withRouter } from 'react-router-dom';

import Twitter from 'react-icons/lib/ti/social-twitter';
import Github from 'react-icons/lib/ti/social-github';
import Bars from 'react-icons/lib/fa/bars';
import Close from 'react-icons/lib/fa/times-circle';

class ResponsiveNav extends Component {
  state = {
    isOpen: false,
  };

  componentWillMount() {
    this.unlisten = this.props.history.listen(() => {
      this.setState({
        isOpen: false,
      });
    });
  }

  componentWillUnmount() {
    this.unlisten();
  }

  switchBar = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    return (
      <nav className="responsive-nav">
        <div className="responsive-nav__bars" onClickCapture={this.switchBar}>
          {this.state.isOpen ? <Close size="4rem" /> : <Bars size="4rem" />}
        </div>
        <div className="responsive-nav__logo--container">
          <Link to="/" className="responsive-nav__logo">
            Mostafa hesham
          </Link>
        </div>
        <div
          className={
            this.state.isOpen
              ? 'responsive-nav__items'
              : 'responsive-nav__items--hidden'
          }
        >
          <NavLink
            to="/profile"
            className="responsive-nav__item"
            activeClassName="responsive-nav__item--active"
          >
            Profile
          </NavLink>
          <NavLink
            to="/projects"
            className="responsive-nav__item"
            activeClassName="responsive-nav__item--active"
          >
            Projects
          </NavLink>
          <a
            className="responsive-nav__item"
            href="https://github.com/mhesham32"
          >
            <Github size="4rem" />
          </a>
          <a
            className="responsive-nav__item"
            href="https://twitter.com/mhesham32"
          >
            <Twitter size="4rem" />
          </a>
        </div>
      </nav>
    );
  }
}

export default withRouter(ResponsiveNav);
