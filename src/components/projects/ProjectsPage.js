import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import Types from 'prop-types';

import Sidebar from '../sidebar/Sidebar';
import Projects from './Projects';

const NotFound = () => <Redirect to="/404" />;

class ProjectsPage extends React.Component {
  static propTypes = {
    history: Types.object.isRequired,
    match: Types.object.isRequired,
    location: Types.object.isRequired,
  };

  state = {
    componentIsReady: false,
  };

  componentWillMount() {
    const { history, match, location } = this.props;
    if (
      location.pathname === '/projects' ||
      location.pathname === '/projects/'
    ) {
      history.push(`${match.path}/all`);
    }
    if (location.pathname.includes('/projects/')) {
      this.setState({ componentIsReady: true });
    }
  }

  componentDidUpdate(prevProps) {
    // to prevent the redirect to 404 before the projectsPage get rendered

    /*  eslint-disable */
    if (this.props.location !== prevProps.location) {
      this.setState({ componentIsReady: true });
    }
    /*  eslint-enable */
  }

  render() {
    return (
      <React.Fragment>
        <div className="projects-page">
          <Sidebar />
          {this.state.componentIsReady && (
            <Switch>
              <Route exact path="/projects/:projectType" component={Projects} />
              <Route component={NotFound} />
            </Switch>
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default ProjectsPage;
