import React from 'react';

const NoProjects = () => (
  <div
    style={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    }}
  >
    <h1>Sorry can't find any projects with your specifications!</h1>
    <img
      src="https://octodex.github.com/images/minion.png"
      alt=""
      className="no-projects"
      style={{
        width: '100%',
        maxWidth: '500px',
      }}
    />
  </div>
);

export default NoProjects;
