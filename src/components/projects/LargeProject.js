import React from 'react';
import { Link } from 'react-router-dom';
import Types from 'prop-types';

class LargeProjectCard extends React.Component {
  static propTypes = {
    headerImage: Types.string.isRequired,
    title: Types.string.isRequired,
    desc: Types.string.isRequired,
    link: Types.string,
  };

  static defaultProps = {
    link: '#',
  };

  // TODO: change the redux state to include the post slug

  render() {
    return (
      <div className="large-project">
        <div className="large-project__content">
          <h1 className="large-project__heading">{this.props.title}</h1>
          <p className="large-project__description">{this.props.desc}</p>
          <div className="large-project__read-more">
            <Link to={`/post/${this.props.slug}`}>Read More</Link>
          </div>
        </div>
        <Link to={`/post/${this.props.slug}`} className="large-project__img">
          <div
            className="large-project__img--preview"
            style={{ backgroundImage: `url(${this.props.headerImage})` }}
          />
        </Link>
      </div>
    );
  }
}

export default LargeProjectCard;
