import React from 'react';
import { Link } from 'react-router-dom';
import Types from 'prop-types';
import { connect } from 'react-redux';

import { selectLatestPosts } from '../../selectors/selectPosts';
import { loadLatestPosts } from '../../actions/posts';
import MiniProject from './MiniProject';
import Loader from '../common/Loader';

class FeaturedProjects extends React.Component {
  static propTypes = {
    projects: Types.array.isRequired,
    loading: Types.bool.isRequired,
    loadLatestPosts: Types.func.isRequired,
  };

  state = {
    numberOfPosts: 3,
  };

  componentWillMount() {
    // 167 is the realtive percentage between the window and the container
    const containerWidth = Math.floor(window.innerWidth / 1.167);
    const numberOfPosts =
      containerWidth > 1700 ? containerWidth / 400 : containerWidth / 330;
    if (numberOfPosts > 1) {
      this.setState({ numberOfPosts });
    } else {
      this.setState({ numberOfPosts: 1 });
    }
    const projectsToLoad = numberOfPosts > 1 ? numberOfPosts : 1;
    this.props.loadLatestPosts(projectsToLoad);
  }

  render() {
    return (
      <section className="latest-projects">
        <h1 className="latest-projects__heading u-heading-primary">
          Featured Projects
        </h1>
        <div className="latest-projects__container">
          {this.props.loading ? (
            <Loader />
          ) : (
            this.props.projects.map((project, i) => (
              <MiniProject
                {...project}
                key={`project#${i + 1}`}
                style={{
                  width: `${(100 + this.state.numberOfPosts * 2) /
                    this.state.numberOfPosts -
                    2}%`,
                }}
              />
            ))
          )}
        </div>
        <div className="u-center">
          <div className="latest-projects__button">
            <Link to="/projects">Show all Projects</Link>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  projects: selectLatestPosts(state.posts, state.filters),
  loading: state.postsMetadata.loading,
});

export default connect(mapStateToProps, { loadLatestPosts })(FeaturedProjects);
