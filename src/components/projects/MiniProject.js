import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const MiniProject = props => (
  <Link to={`/post/${props.slug}`} className="mini-project" style={props.style}>
    <div className="mini-project__img">
      <div
        className="mini-project__img--preview"
        style={{ backgroundImage: `url(${props.headerImage})` }}
      />
    </div>
    <h1 className="mini-project__heading">{props.title}</h1>
    <p className="mini-project__description">{props.desc}</p>
  </Link>
);

MiniProject.propTypes = {
  headerImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  style: PropTypes.object.isRequired,
  slug: PropTypes.string.isRequired,
};

export default MiniProject;
