import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import Types from 'prop-types';
import { Redirect } from 'react-router-dom';
import Card from 'card-vibes';

import selectPosts from '../../selectors/selectPosts';
import LargeProjectCard from './LargeProject';
import NoProjects from './NoProjects';
import { filterByType } from '../../actions/filters';
import Loader from '../common/Loader';

const style = {
  borderRadius: '0.6rem',
  minHeight: '25rem',
  overflow: 'hidden',
  width: '100%',
  marginBottom: '6rem',
};

class Projects extends React.Component {
  static propTypes = {
    posts: Types.array.isRequired,
    filterByType: Types.func.isRequired,
    match: Types.object.isRequired,
    loading: Types.bool.isRequired,
  };

  state = {
    vaildRoutes: false,
  };

  componentWillMount() {
    const { params } = this.props.match;
    const vaildRoutes = [
      'all',
      'static-pages',
      'interactive-apps',
      'fullstack-apps',
    ];

    this.setState({ vaildRoutes: vaildRoutes.includes(params.projectType) });

    this.props.filterByType(params.projectType);
  }

  render() {
    return this.state.vaildRoutes ? (
      <div className="projects-container">
        <div className="projects">
          {/* eslint-disable no-nested-ternary */
          this.props.loading ? (
            <Loader />
          ) : this.props.posts.length < 1 ? (
            <NoProjects />
          ) : (
            <ReactCSSTransitionGroup
              transitionName="example"
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {this.props.posts.map(post => (
                <Card style={style} key={post.createdAt}>
                  <LargeProjectCard {...post} />
                </Card>
              ))}
            </ReactCSSTransitionGroup>
          )}
        </div>
      </div>
    ) : (
      <Redirect to="/404" />
    );
  }
}

const mapStateToProps = state => ({
  posts: selectPosts(state.posts, state.filters),
  loading: state.postsMetadata.loading,
});

export default connect(mapStateToProps, { filterByType })(Projects);
