import React from 'react';
import Types from 'prop-types';
import CheckIcon from 'react-icons/lib/fa/check';

const InputGroup = props => (
  <div className={`${props.className}__group`}>
    <input
      type={props.type}
      checked={props.checked}
      className={`${props.className}__input`}
      id={props.id}
      name={props.name}
      onChange={props.onChange}
    />
    <label htmlFor={props.id} className={`${props.className}__label`}>
      <span className={`${props.className}__button`}>
        {props.type === 'checkbox' && (
          <span className={`${props.className}__check-icon`}>
            <CheckIcon />
          </span>
        )}
      </span>
      <span className={`${props.className}__text-span`}>{props.name}</span>
    </label>
  </div>
);

InputGroup.defaultProps = {
  className: 'sidebar',
};

InputGroup.propTypes = {
  className: Types.string,
  id: Types.string.isRequired,
  name: Types.string.isRequired,
  checked: Types.bool.isRequired,
  type: Types.string.isRequired,
  onChange: Types.func.isRequired,
};

export default InputGroup;
