import React from 'react';
import { withRouter } from 'react-router-dom';
import Types from 'prop-types';
import { connect } from 'react-redux';

import InputGroup from './InputGroup';
import { filterByType, filterByTags } from '../../actions/filters';

class Sidebar extends React.Component {
  static propTypes = {
    history: Types.object.isRequired,
    match: Types.object.isRequired,
    changeProjectType: Types.func.isRequired,
    selectTags: Types.func.isRequired,
  };

  state = {
    radioButtons: {
      checkAll: true,
      checkInterActive: false,
      checkStatic: false,
      checkFullstack: false,
    },
    checkboxes: {
      checkReact: false,
      checkNode: false,
      checkGraphql: false,
      checkMeteor: false,
    },
    tags: [],
  };

  componentWillMount() {
    let param = this.props.history.location.pathname;
    param = param.slice(10).replace(/-/, ' ');
    const radioButtons = { ...this.state.radioButtons };

    Object.keys(radioButtons).map(key => {
      const keyParam = key.toLowerCase().slice(5);
      if (param.includes(keyParam)) {
        radioButtons[key] = true;
      } else {
        radioButtons[key] = false;
      }
      return radioButtons[key];
    });
    this.setState({ radioButtons });
  }

  onChange = ({ target }) => {
    const radioButtons = { ...this.state.radioButtons };
    const checkboxes = { ...this.state.checkboxes };

    if (target.type === 'radio') {
      const routerFunctionality = () => {
        const { history, match, changeProjectType } = this.props;
        const projectsType = target.name.replace(/\s/, '-').toLowerCase();
        history.push(`${match.path}/${projectsType}`);
        Object.keys(checkboxes).map(key => {
          if (this.state.checkboxes[key]) {
            checkboxes[key] = false;
            this.setState({ checkboxes });
          }
          return checkboxes[key];
        });
        this.setState({ checkboxes });
        this.setState({ tags: [] });
        changeProjectType(projectsType);
      };

      Object.keys(radioButtons).map(key => {
        if (key !== target.id) {
          radioButtons[key] = false;
        } else {
          radioButtons[key] = true;
          routerFunctionality();
        }
        return radioButtons[key];
      });
      this.setState({ radioButtons });
    } else if (target.type === 'checkbox') {
      checkboxes[target.id] = !this.state.checkboxes[target.id];
      this.setState({ checkboxes });
      this.filterTags(target).then(tags => {
        this.props.selectTags(tags || ['all']);
      });
    }
  };

  filterTags = target => {
    const tag = target.name.toLowerCase();
    if (this.state.tags.includes(tag)) {
      const tagsArr = [...this.state.tags];

      const notReduntantTags = tagsArr.filter(element => element !== tag);

      this.setState({ tags: notReduntantTags });

      return new Promise(resolve => {
        if (notReduntantTags.length === 0) {
          resolve(['all']);
        }
        resolve(notReduntantTags);
      });
    }
    const tags = [...this.state.tags, tag];
    this.setState({ tags: [...this.state.tags, tag] });
    return new Promise(resolve => {
      resolve(tags);
    });
  };

  render() {
    const { radioButtons, checkboxes } = this.state;

    return (
      <div className="sidebar">
        <form className="sidebar__form">
          <div className="radio-buttons">
            <InputGroup
              type="radio"
              name="all"
              className="radio-buttons"
              checked={radioButtons.checkAll}
              id="checkAll"
              onChange={this.onChange}
            />
            <InputGroup
              type="radio"
              name="Static Pages"
              className="radio-buttons"
              checked={radioButtons.checkStatic}
              id="checkStatic"
              onChange={this.onChange}
            />
            <InputGroup
              type="radio"
              name="Interactive Apps"
              checked={radioButtons.checkInterActive}
              id="checkInterActive"
              className="radio-buttons"
              onChange={this.onChange}
            />
            <InputGroup
              type="radio"
              name="Fullstack Apps"
              className="radio-buttons"
              checked={radioButtons.checkFullstack}
              id="checkFullstack"
              onChange={this.onChange}
            />
          </div>
          <div className="checkboxes">
            <InputGroup
              type="checkbox"
              name="React"
              className="checkboxes"
              checked={checkboxes.checkReact}
              id="checkReact"
              onChange={this.onChange}
            />
            <InputGroup
              type="checkbox"
              name="Node"
              className="checkboxes"
              checked={checkboxes.checkNode}
              id="checkNode"
              onChange={this.onChange}
            />
            <InputGroup
              type="checkbox"
              name="Graphql"
              className="checkboxes"
              checked={checkboxes.checkGraphql}
              id="checkGraphql"
              onChange={this.onChange}
            />
            <InputGroup
              type="checkbox"
              name="Meteor"
              className="checkboxes"
              checked={checkboxes.checkMeteor}
              id="checkMeteor"
              onChange={this.onChange}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default connect(undefined, {
  changeProjectType: filterByType,
  selectTags: filterByTags,
})(withRouter(Sidebar));
