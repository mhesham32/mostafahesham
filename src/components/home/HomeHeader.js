import React from 'react';
import flower from '../../assets/flower.jpg';

const styles = {
  backgroundImage: `linear-gradient(to right bottom,
    rgba(7, 71, 166,0.8),
    rgba(0, 101, 255,0.8)
  ),
  url(${flower})`,
};

const HomeHeader = () => (
  <header className="home-header" style={styles}>
    <h1 className="home-header__heading-primary">Mostafa Hesham</h1>
    <h2 className="home-header__heading-secondary">
      a front-end developer
    </h2>
  </header>
);

export default HomeHeader;
