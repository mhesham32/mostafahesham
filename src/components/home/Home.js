import React from 'react';
import HomeHeader from './HomeHeader';
import SectionInfo from './SectionInfo';
import WhatIDo from '../DoCards/WhatIDo';
import FeaturedProjects from '../projects/FeaturedProjects';
import ContactMe from '../contact/ContactMe';

const Home = () => (
  <React.Fragment>
    <HomeHeader />
    <div className="container">
      <main className="home">
        <SectionInfo />
        <WhatIDo />
        <FeaturedProjects />
        <section className="contact-me">
          <div className="contact-me__container">
            <h1 className="u-heading-primary contact-me__heading">
              Say something
            </h1>
            <ContactMe />
          </div>
        </section>
      </main>
    </div>
  </React.Fragment>
);

export default Home;
