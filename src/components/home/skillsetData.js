import reactLogo from '../../assets/logos/ReactLogo.svg';
import javaScript from '../../assets/logos/javaScript.png';
import html from '../../assets/logos/html5.png';
import css from '../../assets/logos/css3.png';
import responsive from '../../assets/logos/responsive.png';
import sass from '../../assets/logos/sass.png';
import node from '../../assets/logos/node.png';
import graphql from '../../assets/logos/graphql.png';
import jquery from '../../assets/logos/jquery.png';
import bootstrap from '../../assets/logos/bootstrap.png';
import material from '../../assets/logos/material.png';
import appolo from '../../assets/logos/appolo.jpg';
import meteor from '../../assets/logos/meteor.png';
import gatsbyJS from '../../assets/logos/gatasbyjs.png';
import firebase from '../../assets/logos/firebase.png';

export default [
  {
    title: 'react',
    image: reactLogo,
  },
  {
    title: 'javaScript',
    image: javaScript,
  },
  {
    title: 'html 5',
    image: html,
  },
  {
    title: 'css 3',
    image: css,
  },
  {
    title: 'responsive design',
    image: responsive,
  },
  {
    title: 'Sass',
    image: sass,
  },
  {
    title: 'Graphql',
    image: graphql,
  },
  {
    title: 'Node.js',
    image: node,
  },
  {
    title: 'Bootstrap',
    image: bootstrap,
  },
  {
    title: 'Material UI',
    image: material,
  },
  {
    title: 'Jquery',
    image: jquery,
  },
  {
    title: 'appolo',
    image: appolo,
  },
  {
    title: 'gatsbyJS',
    image: gatsbyJS,
  },
  {
    title: 'Meteor',
    image: meteor,
  },
  {
    title: 'firebase',
    image: firebase,
  },
];
