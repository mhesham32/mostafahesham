import React from 'react';
import SkillSet from './SkillSet';

const SectionInfo = () => (
  <section className="section-info">
    <div className="u-center">
      <p className="section-info__description">
        I am a developer who is enthusiastic for making the web a better place ,
        so I create awesome web sites and web applications Hire me to build you
        a website or improve your existing one .
      </p>
    </div>
    <SkillSet />
  </section>
);

export default SectionInfo;
