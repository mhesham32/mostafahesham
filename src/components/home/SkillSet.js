import React from 'react';
import skillsetData from './skillsetData';

const SkillSet = () => (
  <div className="skill-set">
    <h1 className="skill-set__heading">My skillset</h1>
    <div className="skill-set__container">
      {skillsetData.map(item => (
        <Skill key={item.title} image={item.image} title={item.title} />
      ))}
    </div>
  </div>
);

const Skill = ({ title, image }) => (
  <div className="skill-set__container--item">
    <img src={image} alt={title} />
    <h3>{title}</h3>
  </div>
);

export default SkillSet;
