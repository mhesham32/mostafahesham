import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Content extends Component {
  static propTypes = {
    state: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onSelectText: PropTypes.func.isRequired,
  };

  getSelectedText = () => {
    let text = '';
    if (window.getSelection) {
      text = window.getSelection().toString();
    } else if (document.selection && document.selection.type !== 'Control') {
      /* eslint-disable prefer-destructuring */
      text = document.selection.createRange().text;
    }
    this.props.onSelectText(text);
    return text;
  };

  render() {
    return (
      <div>
        <h1 style={{ textAlign: 'center' }}>Content</h1>
        <textarea
          name="content"
          className="admin-form__content"
          value={this.props.state}
          onChange={this.props.onChange}
          onSelect={this.getSelectedText}
          required
        />
      </div>
    );
  }
}
