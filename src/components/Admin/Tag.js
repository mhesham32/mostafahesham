import React from 'react';
import PropTypes from 'prop-types';

const Tag = ({ name }) => (
  <div className="tags__tag">
    <h3>{name}</h3>
  </div>
);

Tag.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Tag;
