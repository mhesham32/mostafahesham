import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Markdown from '../common/Markdown';

const PreviewPost = ({
  title,
  desc,
  headerImage,
  content,
  type,
  uploadError,
  uploadErrorMessage,
  uploadComplete,
  uploadProgress,
  uploadLoading,
}) => (
  <div className="preview-post">
    <div className="upload__view">
      {// eslint-disable-next-line
      uploadLoading ? (
        uploadComplete ? (
          <h1 className="upload__view--complete">
            your image has been uploaded successfuly
          </h1>
        ) : (
          <h1 className="upload__view--loading">
            your image is uploading {uploadProgress}% done!
          </h1>
        )
      ) : uploadError ? (
        <h1 className="upload__view--error">
          something went wrong {uploadErrorMessage}
        </h1>
      ) : null}
    </div>
    <h1>{title}</h1>
    <h3>{desc}</h3>
    <h3 style={{ textAlign: 'lift' }}>
      Project Type :<Link to={`projects/${type}`}>{type}</Link>
    </h3>
    <img src={headerImage} alt="" />
    <Markdown>{content}</Markdown>
  </div>
);

PreviewPost.propTypes = {
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  headerImage: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  uploadError: PropTypes.bool.isRequired,
  uploadErrorMessage: PropTypes.string.isRequired,
  uploadProgress: PropTypes.number.isRequired,
  uploadComplete: PropTypes.bool.isRequired,
  uploadLoading: PropTypes.bool.isRequired,
};

export default PreviewPost;
