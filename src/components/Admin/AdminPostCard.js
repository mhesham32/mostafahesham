import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Modal from 'react-modal';

import { deletePost } from '../../actions/posts';

class AdminPostCard extends React.Component {
  static propTypes = {
    headerImage: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    deletePost: PropTypes.func.isRequired,
  };

  state = {
    modalIsOpen: false,
    isConfirmed: false,
  };

  componentWillUpdate(nextProps, nextState) {
    if (
      nextState.isConfirmed !== this.state.isConfirmed &&
      nextState.isConfirmed
    ) {
      this.props.deletePost(this.props.id).then(() => {
        this.setState({ modalIsOpen: false });
      });
    }
  }

  handleDeletion = () => {
    this.setState({ modalIsOpen: true });
  };

  handleConfirm = () => {
    this.setState({ isConfirmed: true });
  };

  handleCancel = () => {
    this.setState({ modalIsOpen: false });
  };

  renderModal = () => (
    <Modal isOpen={this.state.modalIsOpen} shouldCloseOnOverlayClick>
      <div>
        <p className="ReactModal__Content--paragraph">
          Are you sure you want to delete <br />
          {`"${this.props.title}" ?`}
        </p>
        <div className="ReactModal__Content--buttons">
          <button
            onClick={this.handleConfirm}
            className="ReactModal__Content--confirm"
          >
            Confirm
          </button>
          <button
            onClick={this.handleCancel}
            className="ReactModal__Content--cancel"
          >
            Cancel
          </button>
        </div>
      </div>
    </Modal>
  );

  render() {
    const { id, headerImage, title } = this.props;
    return (
      <div className="post-card">
        {this.renderModal()}
        <div
          className="post-card__image"
          style={{ backgroundImage: `url(${headerImage})` }}
        />
        <h2 className="post-card__title">{title}</h2>

        <div className="post-card__buttons">
          <Link to={`/admin/editor/${id}`} className="post-card__buttons--edit">
            Edit
          </Link>
          <button
            type="button"
            className="post-card__buttons--delete"
            onClick={this.handleDeletion}
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default connect(undefined, { deletePost })(AdminPostCard);
