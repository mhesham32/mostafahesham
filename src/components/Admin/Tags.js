import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TagCheckbox from './TagCheckbox';
import Tag from './Tag';

export default class Tags extends Component {
  static propTypes = {
    addTags: PropTypes.func.isRequired,
    tags: PropTypes.array.isRequired,
  };

  state = {
    react: false,
    graphql: false,
    node: false,
    meteor: false,
    tags: ['react', 'node', 'meteor', 'graphql'],
    newTagName: '',
  };

  componentDidUpdate(prevProps, prevState) {
    const tags = this.state.tags.filter(tag => this.state[tag]);
    if (this.state !== prevState) {
      this.props.addTags(tags);
    }
  }

  handleTagCheckboxChange = ({ target }) => {
    this.setState({
      [target.name]: !this.state[target.name],
    });
  };

  addTag = tagName => {
    this.setState({ tags: [...this.state.tags, tagName] });
  };

  changeInputText = ({ target }) => {
    this.setState({
      newTagName: target.value,
    });
  };

  handleAddTagButton = event => {
    event.preventDefault();
    if (this.state.newTagName !== '') {
      this.addTag(this.state.newTagName);
    }
  };

  render() {
    return (
      <div className="tags">
        <h2>Tags</h2>
        <div className="tags__selected">
          {this.props.tags.map(tag => <Tag name={tag} key={tag} />)}
        </div>
        <button
          className="tags__add-tag"
          name="addTag"
          onClick={this.handleAddTagButton}
        >
          Add Tag
        </button>
        <input
          type="text"
          className="tags__new-tag"
          required
          value={this.state.newTagName}
          onChange={this.changeInputText}
          name="newTag"
        />
        <div className="tags__checkboxes">
          {this.state.tags.map(tag => (
            <TagCheckbox
              checked={this.state[tag]}
              name={tag}
              onChange={this.handleTagCheckboxChange}
              key={tag}
            />
          ))}
        </div>
      </div>
    );
  }
}
