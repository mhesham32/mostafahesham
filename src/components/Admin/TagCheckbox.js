import React from 'react';
import PropTypes from 'prop-types';

const TagCheckbox = ({ checked, name, onChange }) => (
  <div className="tags__checkbox">
    <input
      id={name}
      checked={checked}
      type="checkbox"
      name={name}
      onChange={onChange}
    />
    <label htmlFor={name}>{name}</label>
  </div>
);

TagCheckbox.propTypes = {
  checked: PropTypes.bool.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default TagCheckbox;
