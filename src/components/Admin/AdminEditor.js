import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { addPost, editPost } from '../../actions/posts';
import AdminForm from './AdminForm';
import PreviewPost from './PreviewPost';
import { uploadImage } from '../../actions/uploadImage';

class AdminEditor extends Component {
  static propTypes = {
    addPost: PropTypes.func.isRequired,
    uploadImage: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    headerImage: PropTypes.string.isRequired,
    uploadError: PropTypes.bool.isRequired,
    uploadErrorMessage: PropTypes.string.isRequired,
    uploadProgress: PropTypes.number.isRequired,
    uploadComplete: PropTypes.bool.isRequired,
    uploadLoading: PropTypes.bool.isRequired,
    editPost: PropTypes.func.isRequired,
    draftPosts: PropTypes.array.isRequired,
    posts: PropTypes.array.isRequired,
  };

  state = {
    post: {
      title: '',
      slug: '',
      desc: '',
      tags: [],
      headerImage: '',
      type: 'interactive-apps',
      content: '',
      featured: false,
      githubLink: '',
      previewLink: '',
      images: [],
    },
    selectedText: '',
  };

  componentDidMount() {
    const { id } = this.props.match.params;
    const { posts, draftPosts } = this.props;
    if (posts && draftPosts && id) {
      const allPosts = [...posts, ...draftPosts];
      const post = allPosts.filter(elem => elem.id === id)[0];
      // eslint-disable-next-line
      this.setState({ post });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.headerImage !== nextProps.headerImage) {
      const post = { ...this.state.post };
      post.headerImage = nextProps.headerImage;
      this.setState({ post });
    }
  }

  onTextChange = ({ target }) => {
    if (
      target.type !== 'file' &&
      target.type !== 'checkbox' &&
      target.name !== 'newTag'
    ) {
      const post = { ...this.state.post, [target.name]: target.value };
      post.slug = post.title.replace(/\s/g, '-');
      this.setState({ post });
    }
  };

  onImageUpload = ({ target }) => {
    if (target.type === 'file' && target.files && target.files[0]) {
      const file = target.files[0];
      return this.props
        .uploadImage(file, target.name)
        .then(url => {
          const post = { ...this.state.post };
          const images = { post };
          post.images = [...images, url];
          this.setState({ post });
          return Promise.resolve(url);
        })
        .catch(err => {
          console.log(err.message);
        });
    }
  };

  onSelectText = selectedText => {
    this.setState({ selectedText });
  };

  onUpdatePost = e => {
    e.preventDefault();
    const { id } = this.props.match.params;
    this.props.editPost(id, this.state.post).then(() => {
      this.props.history.push('/admin');
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { id } = this.props.match.params;
    this.props.addPost(this.state.post, e.target.name, id).then(() => {
      this.props.history.push('/');
    });
  };

  checkFeatured = ({ target: { name } }) => {
    console.log('Feature WORK!!');
    const post = { ...this.state.post, [name]: !this.state.post[name] };
    this.setState({ post });
  };

  addTags = arrayOfTags => {
    const post = { ...this.state.post };
    post.tags = arrayOfTags;
    this.setState({ post });
  };

  addMarkdown = (elementType, content, event) => {
    let markdown;
    let finalContent;

    if (typeof elementType === 'string' && elementType !== 'img') {
      switch (elementType) {
        case 'h1':
          markdown = `# ${content}`;
          break;
        case 'h2':
          markdown = `## ${content}`;
          break;
        case 'h3':
          markdown = `### ${content}`;
          break;
        default:
          markdown = `<${elementType}>${content}</${elementType}>`;
      }

      const { content: contentState } = this.state.post;

      if (contentState.includes(content)) {
        finalContent = contentState.replace(content, markdown);
      }
    } else if (
      typeof elementType === 'string' &&
      elementType === 'img' &&
      event.target.type === 'file'
    ) {
      this.onImageUpload(event).then(url => {
        markdown = `![](${url})`;
        if (typeof markdown === 'string' && markdown !== '') {
          let stateContent = this.state.post.content;
          const post = {
            ...this.state.post,
            content: (stateContent += markdown),
            images: [...this.state.post.images, url],
          };
          this.setState({ post });
        }
      });
    }
    if (
      typeof markdown === 'string' &&
      markdown !== '' &&
      typeof finalContent === 'string' &&
      finalContent !== ''
    ) {
      const post = {
        ...this.state.post,
        content: finalContent,
      };
      this.setState({ post });
    }
  };

  render() {
    const { title, desc, tags, headerImage, type, content } = this.state.post;
    return (
      <div className="admin-page">
        <PreviewPost
          title={title}
          desc={desc}
          headerImage={headerImage}
          content={content}
          type={type}
          uploadError={this.props.uploadError}
          uploadErrorMessage={this.props.uploadErrorMessage}
          uploadProgress={this.props.uploadProgress}
          uploadComplete={this.props.uploadComplete}
          uploadLoading={this.props.uploadLoading}
        />
        <AdminForm
          title={title}
          desc={desc}
          tags={tags}
          headerImage={headerImage}
          type={type}
          content={content}
          onTextChange={this.onTextChange}
          onImageUpload={this.onImageUpload}
          addMarkup={this.addMarkdown}
          onSelectText={this.onSelectText}
          selectedText={this.state.selectedText}
          onSubmit={this.onSubmit}
          addTags={this.addTags}
          checkFeatured={this.checkFeatured}
          featuredPost={this.state.post.featured}
          githubLink={this.state.post.githubLink}
          previewLink={this.state.post.previewLink}
          updatePost={this.onUpdatePost}
          isUpdateShown={!!this.props.match.params.id}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts,
  draftPosts: state.draft.posts,
  headerImage: state.imageUpload.headerImage,
  uploadError: state.imageUpload.error,
  uploadErrorMessage: state.imageUpload.errorMessage,
  uploadProgress: state.imageUpload.progress,
  uploadComplete: state.imageUpload.complete,
  uploadLoading: state.imageUpload.loading,
});

export default connect(mapStateToProps, { addPost, uploadImage, editPost })(
  AdminEditor
);
