import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const SidebarItem = ({ type, icon: Icon, addMarkdown, selectedText }) => (
  <Fragment>
    {type === 'img' ? (
      <div className="admin-form__sidebar--item">
        <input
          type="file"
          name="uploadImage"
          id="uploadImage"
          style={{ display: 'none' }}
          onChange={event => addMarkdown(type, selectedText, event)}
        />
        <label
          htmlFor="uploadImage"
          style={{
            height: '100%',
            width: '100%',
            padding: '0 0.55rem',
            cursor: 'pointer',
          }}
        >
          <Icon />
        </label>
      </div>
    ) : (
      <div
        className="admin-form__sidebar--item"
        onClick={event => addMarkdown(type, selectedText, event)}
        role="button"
      >
        <Icon />
      </div>
    )}
  </Fragment>
);

SidebarItem.propTypes = {
  icon: PropTypes.func.isRequired,
  addMarkdown: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
  selectedText: PropTypes.string.isRequired,
};

export default SidebarItem;
