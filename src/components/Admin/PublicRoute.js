import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

import { UID } from '../../actions/constants';

const PublicRoute = ({ isAuthenticated, component: Component, ...rest }) => (
  <Route
    {...rest}
    component={props =>
      isAuthenticated ? (
        <Redirect to="/admin/dashboard/all" />
      ) : (
        <Component {...props} />
      )
    }
  />
);

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.uid === UID,
  };
}

PublicRoute.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  component: PropTypes.element.isRequired,
};

export default connect(mapStateToProps)(PublicRoute);
