import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { startLogin, login, logout } from '../../actions/auth';
import { loadDraftPosts } from '../../actions/posts';
import { firebase } from '../../firebase/firebase';
import { UID } from '../../actions/constants';

class SignIn extends React.Component {
  static propTypes = {
    startLogin: PropTypes.func.isRequired,
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    loadDraftPosts: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.loadDraftPosts();

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        if (user.uid === UID) {
          this.props.login(user.uid);
        } else {
          this.props.history.push('/');
        }
      } else {
        this.props.logout();
      }
    });
  }

  render() {
    return (
      <div className="sign-in">
        <button
          className="sign-in__button"
          onClick={this.props.startLogin}
          type="button"
        >
          Sign in
        </button>
      </div>
    );
  }
}

export default connect(undefined, {
  startLogin,
  login,
  logout,
  loadDraftPosts,
})(SignIn);
