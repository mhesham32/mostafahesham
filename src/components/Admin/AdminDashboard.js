import React, { Component } from 'react';
import { connect } from 'react-redux';
import PorpTypes from 'prop-types';
import { Link, Route, NavLink } from 'react-router-dom';

import { startLogout } from '../../actions/auth';
import DashboardPosts from './DashboardPosts';

class AdminDashboard extends Component {
  static propTypes = {
    startLogout: PorpTypes.func.isRequired,
  };

  render() {
    return (
      <div className="admin-dashboard">
        <div className="admin-dashboard__buttons">
          <button
            className="admin-dashboard__buttons--logout"
            onClick={this.props.startLogout}
          >
            Log Out
          </button>
          <Link
            to="/admin/editor"
            className="admin-dashboard__buttons--new-post"
          >
            New Post
          </Link>
        </div>
        <div className="admin-dashboard__nav">
          <NavLink
            to="/admin/dashboard/all"
            className="admin-dashboard__nav--link"
            activeClassName="admin-dashboard__nav--link--active"
          >
            All
          </NavLink>
          <NavLink
            to="/admin/dashboard/draft"
            className="admin-dashboard__nav--link"
            activeClassName="admin-dashboard__nav--link--active"
          >
            draft
          </NavLink>
          <NavLink
            to="/admin/dashboard/published"
            className="admin-dashboard__nav--link"
            activeClassName="admin-dashboard__nav--link--active"
          >
            published
          </NavLink>
        </div>
        <Route path="/admin/dashboard/:postType" component={DashboardPosts} />
      </div>
    );
  }
}

export default connect(undefined, { startLogout })(AdminDashboard);
