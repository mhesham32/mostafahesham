import React from 'react';
import PropTypes from 'prop-types';
import FaImage from 'react-icons/lib/fa/image';
import FaChain from 'react-icons/lib/fa/chain';
import FaListUl from 'react-icons/lib/fa/list-ul';
import FaListOl from 'react-icons/lib/fa/list-ol';
import FaParagraph from 'react-icons/lib/fa/paragraph';
import SidebarItem from './SidebarItem';

const items = [
  {
    type: 'h1',
    icon: () => 'H1',
  },
  {
    type: 'h2',
    icon: () => 'H2',
  },
  {
    type: 'h3',
    icon: () => 'H3',
  },
  {
    type: 'p',
    icon: FaParagraph,
  },
  {
    type: 'img',
    icon: FaImage,
  },
  {
    type: 'a',
    icon: FaChain,
  },
  {
    type: 'ul',
    icon: FaListUl,
  },
  {
    type: 'ol',
    icon: FaListOl,
  },
];

const AdminSidebar = ({ addMarkup, selectedText }) => (
  <ul className="admin-form__sidebar">
    {items.map(item => (
      <SidebarItem
        key={item.type}
        addMarkdown={addMarkup}
        icon={item.icon}
        type={item.type}
        selectedText={selectedText}
      />
    ))}
  </ul>
);

AdminSidebar.propTypes = {
  addMarkup: PropTypes.func.isRequired,
  selectedText: PropTypes.string.isRequired,
};

export default AdminSidebar;
