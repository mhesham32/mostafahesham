import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Loader from '../common/Loader';
import AdminPostCard from './AdminPostCard';

class DashboardPosts extends Component {
  static propTypes = {
    loading: PropTypes.bool.isRequired,
    draftPosts: PropTypes.array.isRequired,
    publishedPosts: PropTypes.array.isRequired,
    match: PropTypes.object.isRequired,
  };

  static getDerivedStateFromProps(props) {
    const { draftPosts, publishedPosts, match, loading } = props;
    const { postType } = match.params;
    if (!loading) {
      switch (postType) {
        case 'all':
          return { posts: [...draftPosts, ...publishedPosts] };
        case 'draft':
          return { posts: [...draftPosts] };
        case 'published':
          return { posts: [...publishedPosts] };
        default:
          return { posts: [...draftPosts, ...publishedPosts] };
      }
    }
  }

  state = { posts: [] };

  render() {
    return (
      <div className="admin-dashboard__posts">
        {this.props.loading ? (
          <Loader />
        ) : (
          <React.Fragment>
            {this.state.posts.map(post => (
              <AdminPostCard
                key={post.id}
                headerImage={post.headerImage}
                title={post.title}
                id={post.id}
              />
            ))}
          </React.Fragment>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const loading = state.draft.loading && state.postsMetadata.loading;
  return {
    loading,
    draftPosts: state.draft.posts,
    publishedPosts: state.posts,
  };
};

export default connect(mapStateToProps)(DashboardPosts);
