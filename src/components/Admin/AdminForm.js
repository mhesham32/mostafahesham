import React from 'react';
import PropTypes from 'prop-types';

import AdminSidebar from './AdminSidebar';
import Content from './Content';
import Tags from './Tags';

const AdminForm = ({
  title,
  desc,
  type,
  tags,
  content,
  onTextChange,
  onImageUpload,
  addMarkup,
  addTags,
  onSelectText,
  selectedText,
  onSubmit,
  checkFeatured,
  featuredPost,
  previewLink,
  githubLink,
  isUpdateShown,
  updatePost,
}) => (
  <div className="admin-form">
    <AdminSidebar addMarkup={addMarkup} selectedText={selectedText} />
    <form onChange={onTextChange}>
      <div className="admin-form__submit">
        <button
          className="admin-form__submit--publish"
          type="button"
          onClickCapture={onSubmit}
          name="publish"
        >
          Publish
        </button>
        {isUpdateShown && (
          <button
            className="admin-form__submit--update"
            type="button"
            onClickCapture={updatePost}
            name="publish"
          >
            update
          </button>
        )}
        <button
          className="admin-form__submit--draft"
          type="button"
          onClickCapture={onSubmit}
          name="draft"
        >
          save Draft
        </button>
      </div>
      <input
        type="checkbox"
        checked={featuredPost}
        onChange={checkFeatured}
        id="checkFeatured"
        name="featured"
      />
      <label htmlFor="checkFeatured" className="admin-form__checkFeatured">
        Featured Post
      </label>
      <input
        type="text"
        name="title"
        value={title}
        placeholder="title"
        required
      />
      <input
        type="text"
        name="previewLink"
        placeholder="demo link"
        value={previewLink}
        required
      />
      <input
        type="text"
        name="githubLink"
        placeholder="github link"
        value={githubLink}
      />
      <input
        type="text"
        name="desc"
        value={desc}
        placeholder="describtion"
        required
      />
      <select name="type" value={type} onChange={onTextChange} required>
        <option value="static-pages">static-pages</option>
        <option value="interactive-apps">interactive-apps</option>
        <option value="fullstack-apps">fullstack-apps</option>
      </select>
      <input
        type="file"
        name="headerImage"
        id="headerImage"
        onChange={onImageUpload}
        required
      />
      <label htmlFor="headerImage">
        <span>Header Image</span>
      </label>
      <Tags addTags={addTags} tags={tags} />
      <Content
        state={content}
        onChange={onTextChange}
        onSelectText={onSelectText}
      />
    </form>
  </div>
);

AdminForm.propTypes = {
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  featuredPost: PropTypes.bool.isRequired,
  selectedText: PropTypes.string.isRequired,
  tags: PropTypes.array.isRequired,
  content: PropTypes.string.isRequired,
  checkFeatured: PropTypes.func.isRequired,
  onTextChange: PropTypes.func.isRequired,
  onImageUpload: PropTypes.func.isRequired,
  addMarkup: PropTypes.func.isRequired,
  addTags: PropTypes.func.isRequired,
  onSelectText: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  previewLink: PropTypes.string.isRequired,
  githubLink: PropTypes.string.isRequired,
  isUpdateShown: PropTypes.bool.isRequired,
  updatePost: PropTypes.func.isRequired,
};

export default AdminForm;
