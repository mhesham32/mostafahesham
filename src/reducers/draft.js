import * as actionTypes from '../actions/constants';

export default (
  state = { loading: false, error: false, posts: [] },
  action
) => {
  switch (action.type) {
    case actionTypes.REQUEST_DRAFT_POSTS:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.LOAD_DRAFT_POSTS:
      return {
        ...state,
        posts: [...action.allPosts],
        loading: false,
      };
    case actionTypes.LOAD_DRAFT_POSTS_ERROR:
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
};
