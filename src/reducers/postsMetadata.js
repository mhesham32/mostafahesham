import * as actionTypes from '../actions/constants';

export default (
  state = { error: false, loading: false, success: false },
  action
) => {
  switch (action.type) {
    case actionTypes.REQUEST_ALL_POSTS:
      return { ...state, loading: true };
    case actionTypes.LOAD_ALL_POSTS_ERROR:
      return { ...state, error: true, loading: false };
    case actionTypes.LOAD_ALL_POSTS:
      return { ...state, loading: false, success: true };
    default:
      return state;
  }
};
