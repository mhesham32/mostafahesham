import * as actionTypes from '../actions/constants';

const initState = {
  complete: false,
  error: false,
  errorMessage: null,
  headerImage: '',
  uploadImage: '',
  progress: 0,
  loading: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case actionTypes.UPLOAD_ERROR:
      return {
        ...state,
        error: true,
        errorMessage: action.errorMessage,
        loading: false,
      };

    case actionTypes.GET_UPLOAD_PROGRESS:
      return {
        ...state,
        progress: action.progress,
        loading: true,
      };

    case actionTypes.GET_URL_AFTER_COMPLETE:
      return {
        ...state,
        [action.targetName]: action.url,
        loading: false,
      };

    default:
      return state;
  }
};
