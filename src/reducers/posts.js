import * as actionTypes from '../actions/constants';

export default (state = [], action) => {
  switch (action.type) {
    case actionTypes.LOAD_ALL_POSTS:
      return [...state, ...action.allPosts];
    case actionTypes.ADD_POST:
      return [...state, action.post];
    case actionTypes.EDIT_POST:
      return state.map(post => {
        if (post.id === action.id) {
          return {
            ...post,
            ...action.updates,
          };
        }
        return post;
      });
    case actionTypes.DELETE_POST:
      return state.filter(({ id }) => id !== action.id);
    default:
      return state;
  }
};
