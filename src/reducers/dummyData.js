import image from '../assets/flower.jpg';

const data = [
  {
    title: 'Project number one',
    id: 1,
    desc:
      'static React Node, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 1,
    type: 'static-pages',
    tags: ['react', 'node'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'Project number two',
    id: 2,
    desc:
      'static React Graphql, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 2,
    type: 'static-pages',
    tags: ['react', 'graphql'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'the latest post for testing the Post route styles',
    id: 3,
    desc:
      'static Node Graphql, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 3,
    type: 'static-pages',
    tags: ['graphql', 'node'],
    images: [],
    content: `##What is Lorem Ipsum?

    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
    
    # Where does it come from?
    
    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
    `,
    link: ''
  },
  {
    title: 'Project number four',
    id: 4,
    desc:
      'Interactive React Node , consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 4,
    type: 'interactive-apps',
    tags: ['react', 'node'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'Project number one',
    id: 5,
    desc:
      'Interactive React Graphql Node, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 5,
    type: 'interactive-apps',
    tags: ['react', 'graphql', 'node'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'Project number two',
    id: 6,
    desc:
      'Interactive Graphql Node, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 6,
    type: 'interactive-apps',
    tags: ['graphql', 'node'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'Project number three',
    id: 7,
    desc:
      'Fullstack React node, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 7,
    type: 'fullstack-apps',
    tags: ['react', 'node'],
    images: [],
    content: '',
    link: ''
  },
  {
    title: 'Project number four',
    id: 8,
    desc:
      'Fullstack React Node Meteor Graphql, consectetur adipisicing elit. Adipisci cum sit incidunt saepe cupiditate eligendi nesciunt iure, ipsum quasi illo molestias aliquid tempora quo vel! Harum, similique. Quam, molestiae dicta.',
    headerImage: image,
    createdAt: 8,
    type: 'fullstack-apps',
    tags: ['react', 'node', 'meteor', 'graphql'],
    images: [],
    content: '',
    link: ''
  }
];

export default data.map(post => {
  post.slug = post.title.replace(/\s/g, '-');
  return post;
});
