import * as actionTypes from '../actions/constants';

const filtersDefaultState = {
  sortBy: 'type',
  numberOfPosts: 0,
  typeInput: 'all',
  tags: ['all'],
};

export default (state = filtersDefaultState, action) => {
  switch (action.type) {
    case actionTypes.LOAD_FEATURED_POSTS:
      return {
        ...state,
        numberOfPosts: action.number,
      };

    case actionTypes.FILTER_BY_TYPE:
      return {
        ...state,
        sortBy: 'type',
        typeInput: action.postType,
      };

    case actionTypes.FILTER_BY_TAGS:
      return {
        ...state,
        tags: action.tags,
        sortBy: 'tags',
      };

    default:
      return state;
  }
};
