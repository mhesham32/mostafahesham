import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import posts from '../reducers/posts';
import filters from '../reducers/filters';
import imageUpload from '../reducers/imageUpload';
import postsMetadata from '../reducers/postsMetadata';
import auth from '../reducers/auth';
import draft from '../reducers/draft';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
  const store = createStore(
    combineReducers({
      posts,
      filters,
      imageUpload,
      postsMetadata,
      auth,
      draft,
    }),
    composeEnhancers(applyMiddleware(thunk))
  );

  return store;
};
