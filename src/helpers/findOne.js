export function findAll(haystack, arr) {
  let isContained = true;
  haystack.map(element => {
    if (!arr.includes(element)) {
      isContained = false;
    }
    return element;
  });
  return isContained;
}
