export default ({ postsMetadata: { loading } }, post) => {
  if (!loading) {
    const date = post.createdAt
      .toDate()
      .toString()
      .substring(0, 16);
    return date;
  }
};
