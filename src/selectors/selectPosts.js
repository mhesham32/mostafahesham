import { findAll } from '../helpers/findOne';

export default (posts, { sortBy, typeInput, tags }) => {
  if (sortBy === 'type') {
    if (typeInput === 'all') {
      return posts;
    }
    return posts.filter(post => typeInput === post.type);
  }
  if (sortBy === 'tags') {
    if (tags[0] === 'all') {
      if (typeInput === 'all') {
        return posts;
      }
      return posts.filter(post => typeInput === post.type);
    }
    if (typeInput === 'all' && tags[0] !== 'all') {
      return posts.filter(post => findAll(tags, post.tags));
    }
    return posts
      .filter(post => typeInput === post.type)
      .filter(post => findAll(tags, post.tags));
  }
};

export const selectLatestPosts = (posts, { numberOfPosts }) =>
  posts.filter(post => post.featured).sort();
