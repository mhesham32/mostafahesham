import { firebase, googleAuthProvider } from '../firebase/firebase';
import * as actionTypes from './constants';

export const login = uid => ({ type: actionTypes.LOGIN, uid });
export const logout = () => ({ type: actionTypes.LOGIN });

export const startLogin = () => () =>
  firebase.auth().signInWithPopup(googleAuthProvider);

export const startLogout = () => () => firebase.auth().signOut();
