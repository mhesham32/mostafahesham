import {
  publishedPosts,
  draftPosts,
  timestamp,
  firebase,
} from '../firebase/firebase';
import * as actionTypes from './constants';

export const loadAllPostsSuccess = (allPosts = []) => ({
  type: actionTypes.LOAD_ALL_POSTS,
  allPosts,
});

const loadAllPostsError = () => ({ type: actionTypes.LOAD_ALL_POSTS_ERROR });
const requestAllPosts = () => ({ type: actionTypes.REQUEST_ALL_POSTS });

export const loadDraftPostsSuccess = (allPosts = []) => ({
  type: actionTypes.LOAD_DRAFT_POSTS,
  allPosts,
});

const loadDraftPostsError = () => ({
  type: actionTypes.LOAD_DRAFT_POSTS_ERROR,
});
const requestDraftPosts = () => ({ type: actionTypes.REQUEST_DRAFT_POSTS });

export const loadLatestPosts = num => ({
  type: actionTypes.LOAD_FEATURED_POSTS,
  number: num,
});

const addPostSuccess = post => ({
  type: actionTypes.ADD_POST,
  post: { ...post },
});

const editPostSuccess = (id, updates = {}) => ({
  type: actionTypes.EDIT_POST,
  id,
  updates,
});

export const deletePostSuccess = id => ({ type: actionTypes.DELETE_POST, id });

// this function adds post to firestore and move posts between collection
export const addPost = (post, submitType, id) => (dispatch, getState) => {
  let databaseCollection = draftPosts;
  const isDraft = !!getState().draft.posts.find(elem => elem.id === id);
  if (submitType === 'publish') {
    databaseCollection = publishedPosts;
  }
  return new Promise((resolve, reject) => {
    const ref = databaseCollection.doc();
    // here we add a normal new post depending on what button was trigered
    ref
      .set({ ...post, id: ref.id, createdAt: timestamp })
      .then(() => {
        ref.get().then(doc => {
          if (doc && doc.exists) {
            resolve(doc.data());
            dispatch(addPostSuccess(doc.data()));
          }
        });
        // here we make sure if the post isn't new and remove it from
        // the collection it was in because we can't move files in firestore
        if (id && isDraft && databaseCollection === publishedPosts) {
          draftPosts
            .doc(id)
            .delete()
            .then(() => {
              console.log('Deleted post successfuly');
            })
            .catch(err => {
              console.log(err.message);
            });
        } else if (id && !isDraft && databaseCollection === draftPosts) {
          publishedPosts
            .doc(id)
            .delete()
            .then(() => {
              console.log('Deleted post successfuly');
            })
            .catch(err => {
              console.log(err.message);
            });
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

export const loadAllPosts = () => dispatch => {
  dispatch(requestAllPosts());
  // this function loads the publish posts only
  publishedPosts.get().then(
    snapshot => {
      const data = snapshot.docs.map(doc => doc.data());
      dispatch(loadAllPostsSuccess(data));
    },
    err => dispatch(loadAllPostsError())
  );
};

export const loadDraftPosts = () => dispatch => {
  dispatch(requestDraftPosts());
  draftPosts.get().then(
    snapshot => {
      const data = snapshot.docs.map(doc => doc.data());
      dispatch(loadDraftPostsSuccess(data));
    },
    err => dispatch(loadDraftPostsError())
  );
};

export const editPost = (id, updates) => (dispatch, getState) => {
  // to know where this doc Parent collectin we use isPublished Bool
  const isPublished = !!getState().posts.find(elem => elem.id === id);
  const docRef = isPublished ? publishedPosts : draftPosts;
  return docRef
    .doc(id)
    .update({ ...updates })
    .then(() => {
      if (docRef === publishedPosts) {
        dispatch(editPostSuccess(id, updates));
      }
    })
    .catch(err => console.log(err.message));
};

export const deletePost = id => (dispatch, getState) => {
  // to know where this doc Parent collectin we use isPublished Bool
  const isPublished = !!getState().posts.find(elem => elem.id === id);
  const docRef = isPublished ? publishedPosts : draftPosts;
  return docRef
    .doc(id)
    .delete()
    .then(() => {
      // every post has images hosted on firestorage and these images are saved in the
      // post images property .we delete those images as the post gets deleted too.
      if (docRef === draftPosts) {
        const draftPost = getState().draft.posts.find(elem => elem.id === id);
        if (draftPost.images) {
          draftPost.images.forEach(img => {
            firebase
              .storage()
              .refFromURL(img)
              .delete()
              .then(() => {
                console.log('File deleted successfully');
              })
              .catch(err => {
                console.log(err.message);
              });
          });
        }
      }
      if (docRef === publishedPosts) {
        const post = getState().posts.find(elem => elem.id === id);
        if (post.images) {
          post.images.forEach(img => {
            firebase
              .storage()
              .refFromURL(img)
              .delete()
              .then(() => {
                console.log('File deleted successfully');
              })
              .catch(err => {
                console.log(err.message);
              });
          });
        }
        dispatch(deletePostSuccess(id));
      }
    })
    .catch(err => console.log(err.message));
};
