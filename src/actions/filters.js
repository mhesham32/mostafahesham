import * as actionTypes from './constants';

export const filterByType = postType => ({
  type: actionTypes.FILTER_BY_TYPE,
  postType,
});

export const filterByTags = (tags = ['all']) => ({
  type: actionTypes.FILTER_BY_TAGS,
  tags,
});
