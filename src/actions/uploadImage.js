import * as actionTypes from './constants';
import { headerImageRef, postImageRef } from '../firebase/firebase';

const completeUpload = (url, targetName) => ({
  type: actionTypes.GET_URL_AFTER_COMPLETE,
  url,
  targetName,
});

const uploadError = err => ({
  type: actionTypes.UPLOAD_ERROR,
  errorMessage: err.message,
});

const uploadProgressSuccess = progress => ({
  type: actionTypes.GET_UPLOAD_PROGRESS,
  progress,
});

export const uploadImage = (photoFile, targetName) => dispatch => {
  let firebaseRef;

  if (targetName === 'headerImage') {
    firebaseRef = headerImageRef;
  } else if (targetName === 'uploadImage') {
    firebaseRef = postImageRef;
  } else {
    return;
  }

  return new Promise((resolve, reject) => {
    const task = firebaseRef(photoFile.name).put(photoFile);
    task.on(
      'state_changed',
      snapshot => {
        const progress = Math.round(
          snapshot.bytesTransferred / snapshot.totalBytes * 100
        );
        console.log(`Upload is ${progress}% done`);
        dispatch(uploadProgressSuccess(progress));
      },
      err => {
        dispatch(uploadError(err));
        reject();
      },
      () => {
        task.snapshot.ref.getDownloadURL().then(url => {
          resolve(url);
          dispatch(completeUpload(url, targetName));
        });
      }
    );
  });
};
