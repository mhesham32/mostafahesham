import React from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import Nav from './components/navigation/Nav';
import Home from './components/home/Home';
import NotFound from './components/NotFound';
import configureStore from './store/configureStore';
import ProjectsPage from './components/projects/ProjectsPage';
import AdminEditor from './components/Admin/AdminEditor';
import AdminDashboard from './components/Admin/AdminDashboard';
import AdminLogin from './components/Admin/SignIn';
import Post from './components/post/Post';
import { loadAllPosts } from './actions/posts';
import ScrollToTop from './components/ScrollToTop';
import PrivateRoute from './components/Admin/PrivateRoute';
import PublicRoute from './components/Admin/PublicRoute';
import Profile from './components/profile/Profile';

const store = configureStore();
store.dispatch(loadAllPosts());

const Routes = () => (
  <Provider store={store}>
    <BrowserRouter>
      <ScrollToTop>
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/post/:slug" component={Post} />
          <Route path="/projects" component={ProjectsPage} />
          <Route path="/profile" component={Profile} />
          <PublicRoute exact path="/admin" component={AdminLogin} />
          <PrivateRoute path="/admin/dashboard" component={AdminDashboard} />
          <PrivateRoute exact path="/admin/editor" component={AdminEditor} />
          <PrivateRoute path="/admin/editor/:id" component={AdminEditor} />
          <Route component={NotFound} />
        </Switch>
      </ScrollToTop>
    </BrowserRouter>
  </Provider>
);

export default Routes;
