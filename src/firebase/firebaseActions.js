import { photoStorageRef } from './firebase';

export const uploadPhoto = (photoFile, progress, error, complete) => {
  const task = photoStorageRef(photoFile.name).put(photoFile);
  task.on(
    'state_changed',
    snapShot => {
      progress(snapShot);
      console.log('PROGRESS');
    },
    err => error(err),
    () => complete(task)
  );
};
