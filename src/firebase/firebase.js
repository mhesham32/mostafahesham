import firebase from 'firebase/app';
import 'firebase/storage';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyBPSL4hKbl1m2pIIp-BMvHpdXlKcLCV6tQ',
  authDomain: 'mostafa-e00e3.firebaseapp.com',
  databaseURL: 'https://mostafa-e00e3.firebaseio.com',
  projectId: 'mostafa-e00e3',
  storageBucket: 'mostafa-e00e3.appspot.com',
  messagingSenderId: '525595254222',
};

firebase.initializeApp(config);

const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

export const timestamp = firebase.firestore.FieldValue.serverTimestamp();
export const headerImageRef = fileName =>
  firebase.storage().ref(`headerImages/${fileName}`);

export const postImageRef = fileName =>
  firebase.storage().ref(`postsImages/${fileName}`);

export const publishedPosts = firestore.collection('published');

export const draftPosts = firestore.collection('draft');

export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();

export { firebase, firestore };
