# Simple Wordpress React template

since Wordpress released the their [headless API](https://developer.wordpress.org/rest-api/) service every thing becomes different than before and more excited. almost 30% of the is using wordpress this means there is a large number of people are familiar with it because of its popularity and it is user friendly but there were limitations :-( .

**if you wish to use Wordpress** you have to work with certain language and environment  
and it is only available on the web and you can't use any of the frameworks or language that you love **But Now** every thing is different ;) .

You can use the power and popularity of Wordpress in any platform with any language and framework using the [rest API](https://developer.wordpress.org/rest-api/) and that is what i did here in this simple project.

this template is fetching data from the Wordpress API using the `redux` library and `redux-thunk` middleware and react which is responsible for viewing the UI depending on the `Store` data that came from `redux` that is pretty all what this template do but i will explain it more in the next few lines .

> i'm not willing to use `redux` in my next react projects as the new features that added to react like `Context` and `Hooks` i'd like to use react alone and handle its state by it self .

Let's begin with the UI which is made with Bootstrap library for simplicity . what this nav bar contains are the categories from the wordpress back-end and if a category has nested categories (children) the the nav bar item will be dropdown menu as long the data is loading the nav bar shows loading indicators which is good for UX

![loading nav bar](https://firebasestorage.googleapis.com/v0/b/mostafa-e00e3.appspot.com/o/postsImages%2FScreenshot_2018-10-28%20React%20Wordpress%20template.png?alt=media&token=979aab4b-d745-49c4-b0ef-ac99e771ef0f)

there how is the is showing  before the data is fetched 

![nav bar](https://firebasestorage.googleapis.com/v0/b/mostafa-e00e3.appspot.com/o/postsImages%2FScreenshot_2018-10-29%20React%20Wordpress%20template.png?alt=media&token=bff6e59a-a66c-474f-a7d2-072f2ac80ea5)

and here the categories are fetched and displayed as nav bar items 

the same thing is used with posts I put a post like animation to replace the actual post 


![loading posts placeholder](https://firebasestorage.googleapis.com/v0/b/mostafa-e00e3.appspot.com/o/postsImages%2FScreenshot_2018-10-29%20React%20Wordpress%20template(3).png?alt=media&token=e35a4f16-3957-4373-9b23-0cffb9529c68)

wordpress sends 10 posts as response by page that is the default settings you can change it in my case I see 10 posts per page is good but what if the blog have more than 10 posts here come the Pagination 

![pagination](https://firebasestorage.googleapis.com/v0/b/mostafa-e00e3.appspot.com/o/postsImages%2FScreenshot_2018-10-29%20React%20Wordpress%20template(1).png?alt=media&token=c523cfe6-8a17-465d-9b44-c778cbf73040)

if the requested slug used in the API call have pages more than one this Pagination `Component` will show up it is reusable for each case as it handles fetching the next page of posts of the Parent Component as I used it in the Search `Component` as well you can see the Pagination code on [github](https://github.com/mhesham32/react-wordpress-template/blob/master/src/components/shared/Pagination.js) 

## Search `Component`

Searching is handled by two components `SearchButton` as I called but it is actually contains the search form and `Search` Component which is stateless component getting the search data from `redux` and the search results if founded what handles the searching it self in `SearchButton`. if the search results are more than one page then the `Pagination` shows up when click next it fetches the next page results of the text used in searching.

   ![Search Component](https://firebasestorage.googleapis.com/v0/b/mostafa-e00e3.appspot.com/o/postsImages%2FScreenshot_2018-10-29%20React%20Wordpress%20template(2).png?alt=media&token=003adfcf-947a-4ab6-b4ca-1df9e1779385)

**that is it for this project see you in a next one** :heart: